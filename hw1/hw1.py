def read_data():
	rainList = []
	dictLine = {}
	data = open("rainfall.csv","r")
	test1 = str(data.readline())
	for line in data:
		currentLine = line.split(",") 
		dictLine = {"id":int(currentLine[0]), "year":int(currentLine[1]), "rain":float(currentLine[2])}
		rainList.append(dictLine)
	data.close()
	return rainList

def dates(data, start=None, end=None):
	listOfDates = []
	
	if (start != None and end == None)  : 
		for years in data : 
			if years["year"] >= start : 
				listOfDates.append(years) 
	
	elif (end != None and start == None):
		for years in data : 
			if years["year"] <= end : 
				listOfDates.append(years)
	elif (end != None and start != None): 
		for years in data : 
			if years["year"] >= start and years["year"] <= end : 
				listOfDates.append(years)
	elif (end == None and start == None): 
		return data 
	return listOfDates 

def paginate(data, offset=None, limit=None):
	listOfDates = []
	
	if (offset != None and limit == None)  : 
		listOfDates = data[offset:] 

	elif (limit != None and offset == None):
		listOfDates = data[:limit]

	elif (limit != None and offset != None): 
		if offset+limit-1 <= len(data)-1 : 
			listOfDates = data[offset:offset+limit]
		else : 
			listOfDates = data[offset:len(data)]

	elif (limit  == None and offset == None): 
		return data 

	return listOfDates 
